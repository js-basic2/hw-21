'use strict';

const btn = document.querySelector('.btn');

btn.addEventListener('click', function (event) {// кнопка "намалювати коло"
  event.preventDefault();
    btn.disabled = true;
    console.log(btn.disabled);
    
  const input = document.createElement('input');       // инпут с полем для ввода диаметра
  input.className = 'input';
  document.body.append(input);
  input.style.cssText = `width: 200px;
  height: 50px;
  margin: 10px auto 10px;
  border-radius: 10px;
  border: 1px solid grey;
  display: flex;
  justify-content: center;
  align-items: center;
  `
  input.setAttribute('placeholder', 'Введіть діаметр кола');
  input.addEventListener('change', function (event) { // появление кнопки "намалювати"
    let trueValue = parseInt(input.value, 10);
    if (!trueValue || trueValue < 0) {
      alert('Введіть число більше 1');
      input.value = '';
    }
    else {

      let enter = document.createElement('button');
      enter.className = 'enter';
      enter.innerHTML = 'Намалювати';
      enter.style.cssText = `
    border-radius: 10px;
    border: 1px solid grey;
    background-color: #D4AED8;
    width: 200px;
  height: 50px;
  margin: auto;
  display: flex;
  justify-content: center;
  align-items: center;`
      document.body.append(enter);


      enter.addEventListener('click', function () {  // появление сетки для кругов
        let div = document.createElement('div');
        div.className = 'circle-wrapper';
        div.style.cssText = `
display: grid;
margin: 0 auto;
width: auto;
height: auto;
gap: 10px;
grid-template-columns: repeat(10, 1fr);
grid-template-rows: repeat(10, 1fr);
  `
        document.body.append(div);

        let count = 100;                       // coздаю 100 кругов;
        for (let i = 1; i <= count; i++) {
          let circle = document.createElement('span');
          circle.className = 'circle';
          div.append(circle);
          circle.style.cssText = `
border-radius: 100%;
width: ${trueValue}cm;
height: ${trueValue}cm;
border: 1px solid grey;
  `
        }

        let reset = document.createElement('button');    // появляется кнопка "очистити"
        reset.innerHTML = 'Очистити';
        reset.className = 'reset';
        reset.style.cssText = `
border-radius: 10px;
    border: 1px solid grey;
    background-color: #D4AED8;
    width: 200px;
  height: 50px;
  margin: 5px auto 5px;
  display: flex;
  justify-content: center;
  align-items: center;
`
        enter.after(reset);




        let span = document.querySelectorAll('.circle');            // делаю массив из кругов и каждому даю цвет
        let arrCircle = Array.from(span);
        arrCircle.forEach((item) => item.style.backgroundColor = `rgb(${randomColor(0, 255)}, ${randomColor(0, 255)}, ${randomColor(0, 255)})`);



        reset.addEventListener('click', function () {           // если нажимаем очистить, скрываем круги и все кнопки.
          div.remove();
          input.remove();
          reset.remove();
          enter.remove();
          btn.disabled=false; 
         

        })

        let elem = document.querySelector('.circle-wrapper');      // удаляем круг при нажатии
        elem.addEventListener('click', function (event) {
          let target = event.target;

          if (target.closest('.circle')) {
            target.remove();
          }
        })

      })

    }
  })


})


function randomColor(min, max) {                                    // функция для изменения цвета каждого круга 
  return Math.ceil(Math.random() * (max - min) - min);

}








